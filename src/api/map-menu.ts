import {MenuLinkProps} from "../components/MenuLink";
import {PageData} from "../templates/Home";

export const mapMenu = (menu = {} as any):PageData['menu'] => {
  const {
    new_tab: newTab = false,
    logo_text: text = '',
    logo_link: link = '',
    menu: links = []
  } = menu;
  return {
    newTab,
    text,
    link,
    links: mapMenuLinks(links)
  }

};

export const mapMenuLinks = (links = []):MenuLinkProps[] => {
  return links.map((item): MenuLinkProps => {
    const { new_tab: newTab = false, link_text: children = '', url: link = ''} = item;
    return {
      children,
      link,
      newTab
    }
  })
}
