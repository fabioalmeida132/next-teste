import Head from 'next/head'
import {Base} from '../Base'
import config from "../../config"
import {theme} from '../../styles/theme'

import {GridTwoColumn, GridTwoColumnProps} from "../../components/GridTwoColumn";
import {GridContent, GridContentProps} from "../../components/GridContent";
import {GridText, GridTextProps} from "../../components/GridText";
import {GridImage, GridImageProps} from "../../components/GridImage";
import {LogoLinkProps} from "../../components/LogoLink";
import {MenuLinkProps} from "../../components/MenuLink";

export type PageData = {
  title: string,
  slug: string,
  footerHtml: string,
  menu: LogoLinkProps & { links: MenuLinkProps[]},
  sections: SectionProps[]
}

export type SectionProps = GridImageProps | GridTextProps | GridTwoColumnProps | GridContentProps;

export type HomeProps = {
  data: PageData[]
}

function Home({data}: HomeProps) {


  const {menu, sections, footerHtml,slug, title } = data[0];
  const {links, text, link, srcImg} = menu;

  return (
    <Base links={links} footerHtml={footerHtml} logoData={{text, link, srcImg}}>
      <Head>
        <title>{title} | {config.siteName}</title>
        <meta name="description" content="Página principal do site"/>
        <meta name="theme" content={theme.colors.primaryColor}/>
      </Head>
      {sections.map((section,index) => {
        const {component} = section;
        const key = `${slug}-${index}`
        if(component === 'section.section-two-columns'){
          return <GridTwoColumn {...section as GridTwoColumnProps} key={key}/>
        }

        if(component === 'section.section-content'){
          return <GridContent {...section as GridContentProps} key={key}/>
        }

        if(component === 'section.section-grid-text'){
          return <GridText {...section as GridTextProps} key={key}/>
        }

        if(component === 'section.section-grid-image'){
          return <GridImage {...section as GridImageProps} key={key}/>
        }
        return '';
      })}
    </Base>
  )
}

export default Home;
