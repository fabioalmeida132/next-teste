import * as S from "./styles";
import {Menu} from "../../components/Menu";
import {Footer} from "../../components/Footer";
import {GoTop} from "../../components/GoTop";
import {MenuLinkProps} from "../../components/MenuLink";
import {ReactNode} from "react";
import {LogoLinkProps} from "../../components/LogoLink";

export type BaseProps = {
  children: ReactNode,
  links: MenuLinkProps[],
  footerHtml: string,
  logoData: LogoLinkProps
};


export const Base = ({links = [], logoData, footerHtml, children}: BaseProps) => {
  return (
    <>
      <Menu links={links} logoData={logoData}/>
      <S.Container>
        {children}
        <Footer footerHtml={footerHtml}/>
      </S.Container>
      <GoTop/>
    </>

  );
};

